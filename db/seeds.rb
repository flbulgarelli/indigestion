Link.transaction do
  10.times do
    curator = Curator.create! username: Faker::Internet.user_name,
                              email: Faker::Internet.email,
                              password: '12345678'
    20.times do
      Link.create! title: Faker::Lorem.sentence(3),
                   url: Faker::Internet.url,
                   description: Faker::Lorem.sentence(28),
                   curator: curator
    end
  end

  Curator.create! username: 'sample',
                  email: 'sample@sample.com',
                  password: '12345678'

end