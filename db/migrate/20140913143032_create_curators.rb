class CreateCurators < ActiveRecord::Migration
  def change
    create_table :curators do |t|
      t.string :username
      t.string :display_name
      t.string :slug

      t.timestamps
    end

    add_index :curators, :slug, unique: true
  end
end
