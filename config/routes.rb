Indigestion::Application.routes.draw do

  root to: 'curators#index'

  devise_for :curators

  resources :curators, only: [:index, :show] do
    scope do
      resources :links, only: :index
    end
    resources :followers, only: [:create, :index]
    resources :followings, only: [:index]
  end

  resource :search, only: :show, controller: :search

  resources :links, only: [:new, :create]
  resources :followings, only: :destroy

end
