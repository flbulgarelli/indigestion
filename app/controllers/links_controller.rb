class LinksController < ApplicationController
  include WithCurator

  before_action :set_curator, only: :index
  before_action :set_current_curator, only: [:create, :new]
  before_action :authenticate_curator!, only: [:create, :new]

  def index
    @links = @curator.recent_links.page(params[:page]).per(25)
  end

  def create
    @link = Link.create params.require(:link).permit(:title, :url, :description).merge(curator: @curator)
    if @link.errors.any?
      flash[:alert] = "Link creation failed: #{@link.errors.first}"
      redirect_to action: :new
    else
      flash[:notice] = 'Link created successfully'
      redirect_to curator_path(@curator)
    end
  end

  def new
    @link = Link.new
  end

  private

  def set_current_curator
    @curator = current_curator
  end
end
