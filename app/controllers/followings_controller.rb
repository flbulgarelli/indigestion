class FollowingsController < ApplicationController
  include WithCurator

  before_action :authenticate_curator!, only: :destroy
  before_action :set_curator

  def destroy
    current_curator.stop_following(@curator)
    flash[:notice] = 'Unfollowed successfully'
    redirect_to :back
  end

  def index
    @follows = @curator.following_follows.page(params[:page]).per(5)
  end
end
