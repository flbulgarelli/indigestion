class ApplicationController < ActionController::Base
  include WithCurrentCurator

  before_filter :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

  helper_method :current_curator_following?
  helper_method :current_curator_is?

  protected

  def configure_permitted_parameters
    [:sign_in, :sign_up, :account_update].each do |it|
      devise_parameter_sanitizer.for(it) {
          |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me)
      }
    end
  end
end
