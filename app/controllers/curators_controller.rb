class CuratorsController < ApplicationController
  include WithCurator

  before_action :set_curator, only: [:show]

  def show
    @links = @curator.recent_links.limit(5)
    @followings = @curator.following_follows
    @followers = @curator.follower_follows
  end

  def index
    @curators = Curator.all
  end
end
