module WithCurrentCurator
  def current_curator_following?(curator)
    curator_signed_in? && current_curator.following?(curator)
  end

  def current_curator_is?(curator)
    curator_signed_in? && current_curator == curator
  end
end