module WithCurator
  def set_curator
    @curator = Curator.friendly.find(params[:curator_id] || params[:id])
  end
end