class SearchController < ApplicationController

  def show
    @searches = PgSearch.multisearch(params.require(:query)).page(params[:page]).per(25)
    @links = @searches.map(&:searchable)
  end
end
