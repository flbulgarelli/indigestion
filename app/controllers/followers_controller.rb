class FollowersController < ApplicationController
  include WithCurator

  before_action :authenticate_curator!, only: :create
  before_action :set_curator

  def create
    current_curator.follow(@curator)
    flash[:notice] = 'Followed successfully'
    redirect_to :back
  end

  def index
    @follows = @curator.follower_follows.page(params[:page]).per(5)
  end
end
