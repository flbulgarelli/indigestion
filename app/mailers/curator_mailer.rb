class CuratorMailer < ActionMailer::Base
  def curated_link_created(link, follower)
    @link = link
    mail(from: "#{link.curator.username}+noreply@indigestion.com", to: follower.email, subject: "[Indigestion] #{link.title} ")
  end
end
