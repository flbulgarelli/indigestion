class Link < ActiveRecord::Base
  include PgSearch
  multisearchable :against => [:title, :description]

  belongs_to :curator

  validates_presence_of :url, :title, :curator

  after_create :notify_followers

  private

  def notify_followers
    curator.notify_followers_of(self)
  end
end
