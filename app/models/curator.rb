class Curator < ActiveRecord::Base
  include WithFollowersNotification
  extend FriendlyId

  has_many :links

  validates_presence_of :username, :email

  friendly_id :username, use: :slugged

  acts_as_followable
  acts_as_follower

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  alias following_follows follows_scoped
  alias follower_follows followers_scoped

  def recent_links
    links.order('id desc')
  end
end
