module WithFollowersNotification
  def notify_followers_of(link)
    followers.each do |follower|
      CuratorMailer.curated_link_created(link, follower).deliver
    end
  end
end