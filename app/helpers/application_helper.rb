module ApplicationHelper
  def curator_digest_title(curator)
    "#{curator.username}'s Digest"
  end

  def curator_followers_title(curator)
    "#{curator.username}'s Followers"
  end

  def curator_followings_title(curator)
    "#{curator.username}'s Followings"
  end
end
